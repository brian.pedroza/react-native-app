import {
    StyleSheet
} from 'react-native';

import Colors from "./Colors";

import {
    widthPercentageToDP,
    heightPercentageToDP
} from '../lib/pxConverter';

import {
    Platform
} from 'react-native';


export default GlobalStyles = StyleSheet.create({
    containerFullCentered:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        flexDirection: 'column'
    }
});
const Direcciones = {
  "General":"9000"
};
const GlobalHost = "http://ec2-3-86-234-211.compute-1.amazonaws.com"
class Api {
    static headers() {
      return {
        'Content-Type': 'application/json'
      }
    }

    static get(direction,route) {
      return this.xhr(direction,route, null, 'GET');
    }
  
    static put(direction,route, params) {
      return this.xhr(direction,route, params, 'PUT')
    }
  
    static post(direction,route, params) {
      return this.xhr(direction,route, params, 'POST')
    }
  
    static delete(direction,route, params) {
      return this.xhr(direction,route, params, 'DELETE')
    }
  
    static xhr(direction,route, params, verb) {
      const host = GlobalHost;
      const url = `${host}:${Direcciones[direction]}/api/${route}`;
      let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
      options.headers = Api.headers()
      return fetch(url, options).then( resp => {
        let json = resp.json();
        if (resp.ok) {
          return json
        }else{
          console.log("Error",resp);
          return {};
        }
        //return json.then(err => {throw err;});
      }).catch( ex => console.log("Error en peticion",ex) );
    }
  }
  export default Api
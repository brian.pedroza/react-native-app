import {Navigation} from 'react-native-navigation';
import Principal from './principal';
import Calendario from './calendario';
import Linterna from './linterna';
import Calculadora from './calculadora';


export function registerScreens(store,provider) {
    Navigation.registerComponentWithRedux('Principal',()=>Principal,provider,store);
    Navigation.registerComponentWithRedux('Calendario',()=>Calendario,provider,store);
    Navigation.registerComponentWithRedux('Linterna',()=>Linterna,provider,store);
    Navigation.registerComponentWithRedux('Calculadora',()=>Calculadora,provider,store);
}
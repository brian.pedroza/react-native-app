import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {ActionCreators} from '../actions/index';
import {bindActionCreators} from 'redux';

import {
    widthPercentageToDP,
    heightPercentageToDP
} from '../lib/pxConverter';

import ModelLogin from '../models/modelLogin';

import {
    View,
    StyleSheet,
    TextInput,
    Text,
    Button,
    Alert,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';

import Colors from '../styles/Colors';
import GlobalStyles from '../styles/GlobalStyles';
import {Navigation} from 'react-native-navigation';


class Principal extends Component{
    static propTypes = {
        componentId: PropTypes.string
    };

    constructor(props){
        super(props);
        this.state = {
            modelLogin: new ModelLogin(),
            strAnswer: null
        }
    }
    fnGoCalculator(){
        Navigation.push(this.props.componentId, {
          component: {
            name: 'Calculadora',
          }
        });
      }
      fnGoCalendar(){
        Navigation.push(this.props.componentId, {
          component: {
            name: 'Calendario',
          }
        });
      }
      fnGoLinterna(){
        Navigation.push(this.props.componentId, {
          component: {
            name: 'Linterna',
          }
        });
      }
    componentDidMount(){
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        if(nextProps.strAnswer != '' && nextProps.strAnswer != this.state.strAnswer){
            this.state.strAnswer = nextProps.strAnswer;
            Alert.alert('Respuesta', nextProps.strAnswer,
                [
                    { text: 'Ok', onPress: () => { }, style: 'cancel' }
                ], { cancelable: false }
            );
        }

    }

    onPressLogin(){
        console.log("ENTRA A FUNCION");
        this.props.Login(this.state.modelLogin['strEmail'],this.state.modelLogin['strPassword']);
    }

    render(){
        return (
            <View style={GlobalStyles.containerFullCentered}>
                <Text style={{color:"#ffffff", fontSize:50, backgroundColor:"#9DABB6",borderRadius:20}}>Bienvenido</Text>
                <TouchableOpacity style={styles.jsnFases} onPress={()=>this.fnGoCalendar()}>
                  <Image style={styles.jsnImgtwo} source= {require('../resources/images/calendario.png')} resizeMode='center'>
                  </Image>
                </TouchableOpacity>
                <TouchableOpacity style={styles.jsnFases} onPress={()=>this.fnGoCalculator()}>
                  <Image style={styles.jsnImgtwo} source= {require('../resources/images/calculadora.png')} resizeMode='center'>
                  </Image>
                </TouchableOpacity>
                <TouchableOpacity style={styles.jsnFases} onPress={()=>this.fnGoLinterna()}>
                  <Image style={styles.jsnImgtwo} source= {require('../resources/images/linterna.png')} resizeMode='center'>
                  </Image>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerInput:{
        width: widthPercentageToDP('70%'),
        borderColor: 'gray',
        borderWidth: 1,
        marginVertical: heightPercentageToDP('1%')
    },
    jsnScrollView: {
        width:widthPercentageToDP('100%')
    },
    jsnFases: {
        width:widthPercentageToDP('100%'),
        height:heightPercentageToDP('16%'),
        borderBottomWidth: 1,
        backgroundColor:'#F2F2F2'
      },
      jsnImgtwo:{
        flex:1,
        height:undefined,
        width:undefined,
      },
});

function mapStateToProps(state){
    return {
        strAnswer: state.strAnswer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Principal);
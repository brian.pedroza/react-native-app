import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {ActionCreators} from '../actions/index';
import {bindActionCreators} from 'redux';

import {
    widthPercentageToDP,
    heightPercentageToDP
} from '../lib/pxConverter';

import ModelLogin from '../models/modelLogin';

import {
    View,
    StyleSheet,
    TextInput,
    Text,
    Button,
    Alert,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';

import Colors from '../styles/Colors';
import GlobalStyles from '../styles/GlobalStyles';
import { Calculator } from 'react-native-calculator'

class Calculadora extends Component{
    static propTypes = {
        componentId: PropTypes.string
    };

    constructor(props){
        super(props);
        this.state = {
            modelLogin: new ModelLogin(),
            strAnswer: null
        }
    }
    componentDidMount(){
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        if(nextProps.strAnswer != '' && nextProps.strAnswer != this.state.strAnswer){
            this.state.strAnswer = nextProps.strAnswer;
            Alert.alert('Respuesta', nextProps.strAnswer,
                [
                    { text: 'Ok', onPress: () => { }, style: 'cancel' }
                ], { cancelable: false }
            );
        }

    }

    onPressLogin(){
        console.log("ENTRA A FUNCION");
        this.props.Login(this.state.modelLogin['strEmail'],this.state.modelLogin['strPassword']);
    }

    render(){
        return (
            <View style={{ flex: 1 }}>
                <Calculator style={{ flex: 1 }} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerInput:{
        width: widthPercentageToDP('70%'),
        borderColor: 'gray',
        borderWidth: 1,
        marginVertical: heightPercentageToDP('1%')
    },
    jsnScrollView: {
        width:widthPercentageToDP('100%')
    },
    jsnFases: {
        width:widthPercentageToDP('100%'),
        height:heightPercentageToDP('16%'),
        borderBottomWidth: 1,
        backgroundColor:'#F2F2F2'
      },
      jsnImgtwo:{
        flex:1,
        height:undefined,
        width:undefined,
      },
});

function mapStateToProps(state){
    return {
        strAnswer: state.strAnswer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Calculadora);
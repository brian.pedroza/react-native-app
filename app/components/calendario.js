import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {ActionCreators} from '../actions/index';
import {bindActionCreators} from 'redux';

import {
    widthPercentageToDP,
    heightPercentageToDP
} from '../lib/pxConverter';

import ModelLogin from '../models/modelLogin';

import {
    View,
    StyleSheet,
    TextInput,
    Text,
    Button,
    Alert,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';

import Colors from '../styles/Colors';
import GlobalStyles from '../styles/GlobalStyles';
import { Calendar } from 'react-native-calendario';

class Calendario extends Component{
    static propTypes = {
        componentId: PropTypes.string
    };

    constructor(props){
        super(props);
        this.state = {
            modelLogin: new ModelLogin(),
            strAnswer: null
        }
    }
    componentDidMount(){
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        if(nextProps.strAnswer != '' && nextProps.strAnswer != this.state.strAnswer){
            this.state.strAnswer = nextProps.strAnswer;
            Alert.alert('Respuesta', nextProps.strAnswer,
                [
                    { text: 'Ok', onPress: () => { }, style: 'cancel' }
                ], { cancelable: false }
            );
        }

    }

    onPressLogin(){
        console.log("ENTRA A FUNCION");
        this.props.Login(this.state.modelLogin['strEmail'],this.state.modelLogin['strPassword']);
    }

    render(){
        return (
            <View style={{flex:1}}>
                <View style={styles.jsnTitle}>
                    <Text style={styles.jsnTexto}>Calendario</Text>
                </View>
                <View style={styles.jsnCalendar}>
                    <Calendar
                    onChange={(range) => console.log(range)}
                    minDate={new Date(2018, 3, 20)}
                    startDate={new Date(2018, 3, 30)}
                    endDate={new Date(2018, 4, 5)}
                    disableRange={true}
                    theme={{
                        activeDayColor: {},
                        monthTitleTextStyle: {
                        color: '#6d95da',
                        fontWeight: '300',
                        fontSize: 16,
                        },
                        emptyMonthContainerStyle: {},
                        emptyMonthTextStyle: {
                        fontWeight: '200',
                        },
                        weekColumnsContainerStyle: {},
                        weekColumnStyle: {
                        paddingVertical: 10,
                        },
                        weekColumnTextStyle: {
                        color: '#b6c1cd',
                        fontSize: 13,
                        },
                        nonTouchableDayContainerStyle: {},
                        nonTouchableDayTextStyle: {},
                        startDateContainerStyle: {},
                        endDateContainerStyle: {},
                        dayContainerStyle: {},
                        dayTextStyle: {
                        color: '#2d4150',
                        fontWeight: '200',
                        fontSize: 15,
                        },
                        dayOutOfRangeContainerStyle: {},
                        dayOutOfRangeTextStyle: {},
                        todayContainerStyle: {},
                        todayTextStyle: {
                        color: '#6d95da',
                        },
                        activeDayContainerStyle: {
                        backgroundColor: '#6d95da',
                        },
                        activeDayTextStyle: {
                        color: 'white',
                        },
                        nonTouchableLastMonthDayTextStyle: {},
                    }}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerInput:{
        width: widthPercentageToDP('70%'),
        borderColor: 'gray',
        borderWidth: 1,
        marginVertical: heightPercentageToDP('1%')
    },
    jsnScrollView: {
        width:widthPercentageToDP('100%')
    },
    jsnFases: {
        width:widthPercentageToDP('100%'),
        height:heightPercentageToDP('16%'),
        borderBottomWidth: 1,
        backgroundColor:'#F2F2F2'
      },
      jsnImgtwo:{
        flex:1,
        height:undefined,
        width:undefined,
      },
      jsnCalendar:{
        width:widthPercentageToDP('100%'),
        height:heightPercentageToDP('95%'),
      },
      jsnTitle:{
        width:widthPercentageToDP('100%'),
        height:heightPercentageToDP('5%'),
      },
      jsnTexto:{
          flex:1,
          backgroundColor:"#9DABB6", 
          color:"#ffffff", 
          textAlign:"center",
          fontSize:30,
        }
});

function mapStateToProps(state){
    return {
        strAnswer: state.strAnswer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Calendario);
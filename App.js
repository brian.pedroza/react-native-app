/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import { createStore, applyMiddleware,compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger'
import reducer from './app/reducers';
import { registerScreens } from "./app/components/screens";
import {Navigation} from 'react-native-navigation';
import { Provider } from 'react-redux';

function configureStore(initialState){
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      logger
    )
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});
registerScreens(store,Provider);

export default class App {
   constructor() {
     Navigation.events().registerAppLaunchedListener(() => {
       Navigation.setRoot({
         root: {
           stack: {
             children: [
               {
                component: {
                   name: 'Principal',
                 }
               }
             ]
           }
         }
       });
     });
   }
 }

 